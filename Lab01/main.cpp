// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#include <iostream>
#include <string>
#include <vector>

#include <huffman/huffman.hpp>
#include <huffman/utility.hpp>

using std::cout;
using std::string;
using std::vector;

int main(int, char**)
{
	string input_string = "Gathering Testing Bat Sitting Radiant Yesterday";

	vector<Encoding> enc_table; // Out variable to store the encoding table
	vector<bool> compressed = compress_string(input_string, &enc_table);
	cout << "Compressed: " << std::noboolalpha << compressed << '\n';
	cout << "Decompressed: " << decompress_string(&compressed, &enc_table) << '\n';

	// Convert to byte array and back as proof of concept
	int padding_count = 0; // Out variable to store the padding count, not required
	vector<uint8_t> vbyte = vbooltovbyte(&compressed, &padding_count);
	vector<bool> vbool = vbytetovbool(&vbyte);
	cout << "Compressed: " << vbool << '\n';
	cout << "Padding: " << padding_count << "\n";
	for(int i = 0; i < padding_count; i++) // Remove padding
	{
		vbool.pop_back();
	}
	cout << "Decompressed: " << decompress_string(&vbool, &enc_table) << "\n";


	return 0;
}
