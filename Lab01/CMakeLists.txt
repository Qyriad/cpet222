cmake_minimum_required(VERSION 3.5)

project(lab01 CXX)

add_executable(main main.cpp)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set_property(TARGET main PROPERTY CXX_STANDARD 17)
set_property(TARGET main PROPERTY CXX_EXTENSIONS false)

target_compile_features(main PRIVATE cxx_lambda_init_captures)
target_compile_features(main PRIVATE cxx_std_17)
target_compile_features(main PRIVATE cxx_auto_type)
target_compile_features(main PRIVATE cxx_binary_literals)
target_compile_features(main PRIVATE cxx_explicit_conversions)
target_compile_features(main PRIVATE cxx_lambdas)
target_compile_features(main PRIVATE cxx_nullptr)
target_compile_features(main PRIVATE cxx_range_for)
target_compile_features(main PRIVATE cxx_reference_qualified_functions)
target_compile_features(main PRIVATE cxx_right_angle_brackets)
target_compile_features(main PRIVATE cxx_rvalue_references)
target_compile_features(main PRIVATE cxx_variable_templates)
target_compile_features(main PRIVATE cxx_template_template_parameters)

add_library(huffman SHARED huffman/utility.cpp huffman/tree.cpp huffman/encoding.cpp huffman/huffman.cpp)

set_property(TARGET huffman PROPERTY CXX_STANDARD 17)
set_property(TARGET huffman PROPERTY CXX_EXTENSIONS false)

if(MSVC)
	set_property(TARGET huffman PROPERTY WINDOWS_EXPORT_ALL_SYMBOLS true)
endif()

target_link_libraries(main huffman)
