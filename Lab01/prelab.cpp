// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01 - Prelab
// License: MIT

#include <iostream>
#include <string>
#include <iomanip>
#include <ios>

using std::cout;
using std::cin;
using std::getline;
using std::string;

void print_binary(unsigned char val);


int main(int, char **)
{
	std::ios default_flag_store(nullptr);
	default_flag_store.copyfmt(cout);

    string buffer;
    getline(cin, buffer);

    // ASCII first...literally just printing it back character by character
    for(char c : buffer)
    {
        cout << c;
    }
    cout << '\n';

    // Hex second
    cout << std::hex << std::uppercase;
    for(char c : buffer)
    {
        static bool first = true;
        if(!first) cout << ", ";
        else first = false;

        cout << "0x" << +c;
    }
    cout << '\n';

	// Reset io manipulation flags--not strictly necessary in this example
	cout.copyfmt(default_flag_store);

    // Binary third
    for(unsigned char c : buffer)
    {
        static bool first = true;
        if(!first) cout << ", ";
        else first = false;
        
        print_binary(c);
    }
    cout << '\n';

    return 0;
}

void print_binary(unsigned char val)
{
    unsigned char mask = 0x80; // 1000 0000
    for(int i = 0; i < 8; i++)
    {
        if(val & mask) cout << "1";
        else cout << "0";

        mask = mask >> 1;
    }
}
