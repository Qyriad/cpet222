// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#include "utility.hpp"

#include <algorithm>

template<typename It, typename T>
std::vector<It> find_all(It begin, It end, T value) // Finds all occurences of a value in a vector
{
	std::vector<It> matches;
	It match = std::find(begin, end, value);
	while(match != end)
	{
		matches.push_back(match);
		match = std::find(match + 1, end, value);
	}
	return matches;
}

std::vector<uint8_t> vbooltovbyte(std::vector<bool> *vbool, int *padding_count) // Pads the end with 0s to be byte aligned, padding_count defaults to nullptr
{
	std::vector<bool> &vec = *vbool;
	std::vector<uint8_t> bytearr;
	// Get the difference between the bitcount and the nearest greater multiple of 8
	// This tells us how many padding bits there will be if the bitcount isn't byte aligned
	if(padding_count) *padding_count = ((vec.size() / 8) + 1) * 8 - vec.size();

	bytearr.reserve(vec.size() / 8);

	for(unsigned int i = 0; i < vec.size(); i += 8)
	{
		uint8_t byte = 0;
		for(unsigned int j = 0; j < 8; j++)
		{
			if (i + j < vec.size())
			{
				byte |= vec[i + j];
			}
			if (j < 7) byte <<= 1; // We don't want to shift if this is the last one and we have no more bits to set
		}
		bytearr.push_back(byte);
	}
	return bytearr;
}

std::vector<bool> vbytetovbool(std::vector<uint8_t> *vbyte)
{
	auto bytetobool = [](uint8_t byte) -> std::vector<bool>
	{
		std::vector<bool> b;
		uint8_t mask = 0b1000'0000;
		for(unsigned int i = 0; i < 8; i++)
		{
			uint8_t resbyte = byte & mask;
			mask >>= 1;
			b.push_back(resbyte >= 1);
		}
		return b;
	};
	std::vector<uint8_t> &vec = *vbyte;
	std::vector<bool> vbool;
	vbool.reserve(vec.size() * 8);
	for(unsigned int i = 0; i < vec.size(); i++)
	{
		auto partial = bytetobool(vec[i]);
		vbool.insert(vbool.end(), partial.begin(), partial.end());
	}
	return vbool;
}

std::ostream &operator<<(std::ostream &out, std::vector<bool> vec)
{
	out << std::noboolalpha;
	for(bool b : vec)
	{
		out << b;
	}
	return out;
}
