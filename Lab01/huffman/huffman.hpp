// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#pragma once

#include <vector>
#include <string>

#include "encoding.hpp"

struct Encoding;

std::vector<bool> compress_string(std::string s, std::vector<Encoding> *enc_table);
std::string decompress_string(std::vector<bool> *compressed_vector, std::vector<Encoding> *encoding_table);
