// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#pragma once

#include <vector>
#include <string>

#include "tree.hpp"

struct Encoding
{
    char c;
    std::vector<bool> enc;
};

void build_enc_table(std::vector<Encoding> *encoding_table, std::vector<bool> current_encoding, Node *current_root);
std::vector<std::vector<Encoding>::iterator> find_partial(std::vector<Encoding>::iterator begin, std::vector<Encoding>::iterator end, std::vector<bool> encoding);
std::vector<Encoding>::iterator find_encoding(std::vector<Encoding> *encoding_table, char c);
char find_encoding(std::vector<Encoding> *encoding_table, std::vector<bool> *compressed_vector, unsigned int *index);
