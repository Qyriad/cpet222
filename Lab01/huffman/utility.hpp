// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#pragma once

#include <cstdint>
#include <type_traits>
#include <vector>
#include <ostream>

template<typename T>
struct remove_cvref // Removes cv and reference qualifiers from a type
{
	typedef std::remove_cv_t<std::remove_reference_t<T>> type;
};
template<typename T>
using remove_cvref_t = typename remove_cvref<T>::type;

template<typename It, typename T>
std::vector<It> find_all(It begin, It end, T value);

std::vector<uint8_t> vbooltovbyte(std::vector<bool> *vbool, int *padding_count = nullptr);
std::vector<bool> vbytetovbool(std::vector<uint8_t> *vbyte);

std::ostream &operator<<(std::ostream &out, std::vector<bool> vec);
