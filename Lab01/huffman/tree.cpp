// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>
#include <iomanip>

#include "tree.hpp"
#include "utility.hpp"

bool count_comp(Node * const &a, Node * const &b)
{
	return (a->count < b->count);
}

bool not_exist(std::vector<Node*> const *vec_to_search, char c)
{
	using V = std::remove_pointer_t<remove_cvref_t<decltype(vec_to_search)>>;
	using T = V::value_type;

	V &vec = *vec_to_search;

	auto lmbda = [c](T const &p) -> bool // lambda required because I need to capture c
	{
		return (c == p->c);
	};

	auto b = std::find_if(vec.begin(), vec.end(), lmbda);
	return (b == vec.end());
}

void build_binary_tree(std::vector<Node*> *sorted_vec)
{
	std::function<int(Node*, Node*)> sum_count = [](Node *a, Node *b) -> int
	{
		return a->count + b->count;
	};
	std::vector<Node*> &vec = *sorted_vec;
	while(vec.size() > 1)
	{
		Node *a = vec[0];
		Node *b = vec[1];
		Node *c = new Node('\0', sum_count(a, b), a, b);
		vec.erase(vec.begin()); // We combined, so length - 1
		vec[0] = c; // But we can reuse the other pointer
		std::sort(vec.begin(), vec.end(), &count_comp);
	}
}

void traverse_print(Node *n, int tab_level)
{
		if(n == nullptr) return;
		traverse_print(n->right, tab_level + 8);
		if(tab_level > 0) std::cout << std::setw(tab_level) << ' ';
		if(n->c == '\0') std::cout << "(\\0, " << n->count << ")\n";
		else std::cout << "(" << n->c << ", " << n->count << ")\n";
		traverse_print(n->left, tab_level + 8);
}

void cascade_delete(Node *root)
{
	if(root == nullptr) return;
	cascade_delete(root->left);
	cascade_delete(root->right);
	delete root;
}
