// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#pragma once

#include <vector>

struct Node
{
	char c;
	int count;
	Node *left;
	Node *right;

	Node(char c, int count, Node *left, Node *right) : c(c), count(count), left(left), right(right) {}
};

bool count_comp(Node * const &a, Node * const &b);
bool not_exist(std::vector<Node*> const *vec_to_Search, char c);
void build_binary_tree(std::vector<Node*> *sorted_vec);
void traverse_print(Node *n, int tab_level);
void cascade_delete(Node *root);
