// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#include <algorithm>

#include "huffman.hpp"

std::vector<bool> compress_string(std::string s, std::vector<Encoding> *enc_table)
{
	std::vector<Node*> sorted_string; // We'll sort by character count
	sorted_string.reserve(s.length()); // It'll definitely be smaller but we don't know by how much
	// Resizing twice is definitely better than resizing even length / 2 times
	for(char c : s)
	{
		if(not_exist(&sorted_string, c)) // Only add unique characters
		{
			int count = std::count(s.begin(), s.end(), c);
			sorted_string.push_back(new Node(c, count, nullptr, nullptr));
		}
	}
	s.shrink_to_fit(); // Second resize
	unsigned int unique_count = sorted_string.size();

	std::sort(sorted_string.begin(), sorted_string.end(), &count_comp); // Sorted by character count
	build_binary_tree(&sorted_string);

	Node *root = sorted_string[0]; // Now we can build the encoding table
	std::vector<Encoding> &table = *enc_table;
	table.reserve(unique_count);
	build_enc_table(&table, std::vector<bool>(), root); // We start with an empty vector for current_encoding

	std::vector<bool> compressed;
	compressed.reserve(unique_count); // That'll cut down on resize operations a bit
	for(char c : s)
	{
		auto enc = find_encoding(&table, c)->enc;
		compressed.insert(compressed.end(), enc.begin(), enc.end());
	}

	cascade_delete(root); // We don't need the tree any more

	return compressed;
}

std::string decompress_string(std::vector<bool> *compressed_vector, std::vector<Encoding> *encoding_table)
{
	std::string dec = "";

	unsigned int index = 0;
	dec += find_encoding(encoding_table, compressed_vector, &index);
	while(index < compressed_vector->size())
	{
		dec += find_encoding(encoding_table, compressed_vector, &index);
	}
	return dec;
}
