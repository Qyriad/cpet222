// Mikaela R.J. Szekely
// NHTI/CPET222
// Lab01
// 02/06/2018
// Credits: https://en.cppreference.com

#include "encoding.hpp"

#include <algorithm>

void build_enc_table(std::vector<Encoding> *encoding_table, std::vector<bool> current_encoding, Node *current_root)
{
	std::vector<Encoding> &table = *encoding_table;
	if(current_root->c != '\0')
	{
		table.push_back(Encoding{current_root->c, current_encoding});
		return;
	}
	std::vector<bool> left_encoding(current_encoding); // Copy contents into left_encoding
	std::vector<bool> right_encoding(current_encoding); // Same for right
	left_encoding.push_back(false);
	right_encoding.push_back(true);
	build_enc_table(encoding_table, left_encoding, current_root->left);
	build_enc_table(encoding_table, right_encoding, current_root->right);
}

std::vector<std::vector<Encoding>::iterator> find_partial(std::vector<Encoding>::iterator begin, std::vector<Encoding>::iterator end, std::vector<bool> encoding)
{
	auto first_partial = [&encoding](Encoding enc) -> bool
	{
		for(unsigned int i = 0; i < encoding.size(); i++)
		{
			if(encoding[i] != enc.enc[i]) return false;
		}
		return true;
	};
	std::vector<std::vector<Encoding>::iterator> matches;
	std::vector<Encoding>::iterator match = std::find_if(begin, end, first_partial);
	while(match != end)
	{
		matches.push_back(match);
		match = std::find_if(match + 1, end, first_partial);
	}
	return matches;
}

std::vector<Encoding>::iterator find_encoding(std::vector<Encoding> *encoding_table, char c)
{
	std::vector<Encoding> &table = *encoding_table;

	for(unsigned int i = 0; i < table.size(); i++)
	{
		Encoding &enc = table[i];
		if(enc.c == c) return table.begin() + i;
	}
	return table.end();
}

char find_encoding(std::vector<Encoding> *encoding_table, std::vector<bool> *compressed_vector, unsigned int *index)
{
	std::vector<Encoding> &table = *encoding_table;
	std::vector<bool> &vec = *compressed_vector;
	unsigned int &part_i = *index;
	std::vector<bool> part;
	part.push_back(vec[part_i]);
	auto res = find_partial(table.begin(), table.end(), part);
	while(res.size() != 1)
	{
		part_i++;
		part.push_back(vec[part_i]);
		res = find_partial(table.begin(), table.end(), part);
	}
	// If we're here we must have only one match
	auto match = res[0];
	part_i++;
	return match->c;
}
