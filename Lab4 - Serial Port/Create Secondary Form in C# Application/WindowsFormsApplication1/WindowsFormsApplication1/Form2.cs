﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /* This secondary form was created by Project - Add Windows Form.
     */
    public partial class Form2 : Form
    {
        public Form1 mainForm;      // needs to be declared public
        private int counter;    // used to count TEST button presses

        public Form2()
        {
            InitializeComponent();
            counter = 1;
         }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();   // closes only the secondar window form
        }

        private void button1_Click(object sender, EventArgs e)
        {
             mainForm.displayMessage("TEST button pressed " + counter++ + " times");
        }
    }
}
