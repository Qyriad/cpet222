﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void displayMessage(string s)
        {
            textBox1.AppendText(s + "\r\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* This event handler will create a secondary form and
             * cause the form to be displayed in a modal fashion. The
             * secondary form will be the controlling form until closed.
             */
            textBox1.Text = "";     // clear Textbox on each button press
            Form2 newWindow = new Form2();
            newWindow.mainForm = this;
            newWindow.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
