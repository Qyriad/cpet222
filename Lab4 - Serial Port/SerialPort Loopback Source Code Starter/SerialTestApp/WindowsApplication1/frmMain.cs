using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CRC8_Class;

namespace WindowsApplication1
{
    public partial class frmMain : Form
    {
		const byte SOH = 0x01;
		const byte EOF = 0xFF;
        public frmMain()
        {
            InitializeComponent();
        }

        private void openPortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                serialPort1.Open();
            }
            else
            {
                MessageBox.Show("Serial Port is already open.");
            }
        }

        private void closePortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
            else
            {
                MessageBox.Show("The serial port must be open before it can be closed.");
            }
        }

		private byte calculateCRC(byte[] data)
		{
			data[data.Length - 1] = 0;
			CRC_Class crc = new CRC_Class(data);
			return Convert.ToByte(crc.crcCalc());
		}

        private void btnWrite_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
				Encoding enc = Encoding.Default;
				byte[] text = enc.GetBytes(boxDataOut.Text); // UTF-16 to ASCII
				byte packet_len = Convert.ToByte(text.Length + 3);
				byte[] output = new byte[packet_len];  // 1 for SOH, 1 for LEN, 1 for CRC
				output[0] = 0x01; // ASCII SOH
				output[1] = packet_len;
				for (int i = 2; i < packet_len - 1; i++) // - 1 to account for CRC byte
				{
					output[i] = text[i - 2];
				}
				output[packet_len - 1] = calculateCRC(output);
				serialPort1.Write(output, 0, packet_len);
            }
            else
            {
                MessageBox.Show("The serial port must be opened before data can be written.");
            }
        }


        private void com1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com1ToolStripMenuItem.Checked = true;
            com2ToolStripMenuItem.Checked = false;
            if (serialPort1.IsOpen)
            {
                MessageBox.Show("The serial port must be closed before changing the COM Port");
            }
            else
            {
                serialPort1.PortName = "COM1";
            }
        }

        private void com2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            com1ToolStripMenuItem.Checked = false;
            com2ToolStripMenuItem.Checked = true;
            if (serialPort1.IsOpen)
            {
                MessageBox.Show("The serial port must be closed before changing the COM Port");
            }
            else
            {
                serialPort1.PortName = "COM2";
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            boxDataRead.Text = "";
        }

		private enum looking_for
		{
			start,
			length,
			payload,
			crc,
			eof
		}

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {

            // Not really possible for this event to be fired if it's not open, but let's test anyway.
            if (serialPort1.IsOpen)
            {
                boxDataRead.Invoke(new EventHandler(delegate
                {
					var state = looking_for.start;
					int available_bytes = serialPort1.BytesToRead;
					string data = "";
					int len = 0;
					byte[] buffer = new byte[serialPort1.ReadBufferSize];
					
					while(available_bytes > 0)
					{
						int b = 0;
						switch(state)
						{
							case looking_for.start:
								b = serialPort1.ReadByte();
								switch(b)
								{
									case SOH:
										state = looking_for.length;
										data = "";
										break;
									case EOF:
									default:
										state = looking_for.start;
										data = "";
										break;
								}
								break;
							case looking_for.length:
								b = serialPort1.ReadByte();
								switch(b)
								{
									case SOH:
										state = looking_for.length;
										data = "";
										break;
								}
								break;
							case looking_for.payload:
								break;
							case looking_for.crc:
								break;
							default:
								// should be impossible
								break;
						}
					}
                    string dataRecieved = serialPort1.ReadExisting();
                    boxDataRead.AppendText(dataRecieved);
                }));
                
            }
            else
            {
                MessageBox.Show("Serial port must be opened before reading data.");
            }
        }
    }
}