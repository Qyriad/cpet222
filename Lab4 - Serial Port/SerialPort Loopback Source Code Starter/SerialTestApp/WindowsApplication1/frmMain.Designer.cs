namespace WindowsApplication1
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.boxDataRead = new System.Windows.Forms.TextBox();
            this.boxDataOut = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnWrite = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closePortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAComToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.com1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.com2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClear = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // boxDataRead
            // 
            this.boxDataRead.Location = new System.Drawing.Point(12, 136);
            this.boxDataRead.Multiline = true;
            this.boxDataRead.Name = "boxDataRead";
            this.boxDataRead.ReadOnly = true;
            this.boxDataRead.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.boxDataRead.Size = new System.Drawing.Size(268, 57);
            this.boxDataRead.TabIndex = 4;
            this.boxDataRead.TabStop = false;
            // 
            // boxDataOut
            // 
            this.boxDataOut.Location = new System.Drawing.Point(12, 55);
            this.boxDataOut.Multiline = true;
            this.boxDataOut.Name = "boxDataOut";
            this.boxDataOut.Size = new System.Drawing.Size(268, 62);
            this.boxDataOut.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Data Recieved:";
            // 
            // btnWrite
            // 
            this.btnWrite.Location = new System.Drawing.Point(12, 201);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(124, 23);
            this.btnWrite.TabIndex = 2;
            this.btnWrite.Text = "Write";
            this.btnWrite.UseVisualStyleBackColor = true;
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Data To Write:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.selectAComToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(292, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openPortToolStripMenuItem,
            this.closePortToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // openPortToolStripMenuItem
            // 
            this.openPortToolStripMenuItem.Name = "openPortToolStripMenuItem";
            this.openPortToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.openPortToolStripMenuItem.Text = "Open Port";
            this.openPortToolStripMenuItem.Click += new System.EventHandler(this.openPortToolStripMenuItem_Click);
            // 
            // closePortToolStripMenuItem
            // 
            this.closePortToolStripMenuItem.Name = "closePortToolStripMenuItem";
            this.closePortToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.closePortToolStripMenuItem.Text = "Close Port";
            this.closePortToolStripMenuItem.Click += new System.EventHandler(this.closePortToolStripMenuItem_Click);
            // 
            // selectAComToolStripMenuItem
            // 
            this.selectAComToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.com1ToolStripMenuItem,
            this.com2ToolStripMenuItem});
            this.selectAComToolStripMenuItem.Name = "selectAComToolStripMenuItem";
            this.selectAComToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.selectAComToolStripMenuItem.Text = "Select a Com";
            // 
            // com1ToolStripMenuItem
            // 
            this.com1ToolStripMenuItem.Checked = true;
            this.com1ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.com1ToolStripMenuItem.Name = "com1ToolStripMenuItem";
            this.com1ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.com1ToolStripMenuItem.Text = "Com 1";
            this.com1ToolStripMenuItem.Click += new System.EventHandler(this.com1ToolStripMenuItem_Click);
            // 
            // com2ToolStripMenuItem
            // 
            this.com2ToolStripMenuItem.Name = "com2ToolStripMenuItem";
            this.com2ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.com2ToolStripMenuItem.Text = "Com 2";
            this.com2ToolStripMenuItem.Click += new System.EventHandler(this.com2ToolStripMenuItem_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(142, 201);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 23);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear Data Recieved";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 228);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnWrite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.boxDataOut);
            this.Controls.Add(this.boxDataRead);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Serial Test App";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox boxDataRead;
        private System.Windows.Forms.TextBox boxDataOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openPortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closePortToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAComToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem com1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem com2ToolStripMenuItem;
        private System.Windows.Forms.Button btnClear;
    }
}

