﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;      // this namespace required for SerialPort class

namespace ConfigureComPortExample
{
    public partial class Form1 : Form
    {
        // class constant definitions
        public const int BIT_RATE_LENGTH = 8;

        // declare instance variables
        private string[] portNames;   //stores names of equipped COM ports 


        // enum declarations used to provision combo boxes
        public Form1()
        {
            InitializeComponent();
            // initialize all of the ComboBoxes shown in GUI
            loadComPortComboBox();
            loadBitRateComboBox();
            loadDataBitComboBox();
            loadStopBitsComboBox();
            loadParityComboBox();
            loadHandshakeComboBox();
        }

        private void loadComPortComboBox()
        {
            // local variable declarations
            int length;

            /* GetPortNames() is a public static method on the SerialPort
             * class so can invoke directly from the SerialPort class. This will
             * produce an array of detected COM ports on this machine (e.g.
             * COM1, COM2, etc
             */
            this.portNames = SerialPort.GetPortNames();

            /* Now initialize the declared comboBox control in the Form with
             * the array of COM port names detected above
             */
            foreach (string name in portNames)
            {
                comboBox1.Items.Add(name);
            }

            // sort the COM port names stored in the comboBox control
            comboBox1.Sorted = true;

            /* lastly, initialize the comboBox displayed text to the first
             * COM port contained in the control. If empty display an empty string.
             */
            length = portNames.Length;
            if (length > 0)
                comboBox1.Text = portNames[length - 1];
            else
                comboBox1.Text = "";    // empty string

        }   // end loadComPortComboBox() definition

        private void loadBitRateComboBox()
        {
            // local variable declarations
            int i;

            /* Initialize BitRateComboBox GUI control
             */
            for (i = 0; i < BIT_RATE_LENGTH; i++)
            {
                switch (i)
                {
                    case 0:
                        comboBox2.Items.Add(1200);
                        break;
                    case 1:
                        comboBox2.Items.Add(2400);
                        break;
                    case 2:
                        comboBox2.Items.Add(4800);
                        break;
                    case 3:
                        comboBox2.Items.Add(9600);
                        break;
                    case 4:
                        comboBox2.Items.Add(14400);
                        break;
                    case 5:
                        comboBox2.Items.Add(28800);
                        break;
                    case 6:
                        comboBox2.Items.Add(33300);
                        break;
                    case 7:
                        comboBox2.Items.Add(56000);
                        break;
                    default:
                        // can't get here
                        break; ;

                }   // end switch statement

            }   // end of for loop

            // sort the COM port names stored in the comboBox control
            //comboBox1.Sorted = true;

            // initialize the text field of the bit rate combo box
            comboBox2.Text = "4800";

        }   // end loadBitRateComboBox() definition

        private void loadDataBitComboBox()
        {
            // local variable declarations

            /* Initialize DataBitComboBox GUI control
             */
            comboBox3.Items.Add(7);
            comboBox3.Items.Add(8);

            // initialize the text field of the data bit combo box
            comboBox3.Text = "8";

        }   // end loadDataBitComboBox() definition

        private void loadStopBitsComboBox()
        {
            // local variable declarations

            /* Initialize StopBitsComboBox GUI control
             */
            comboBox4.Items.Add(1);
            comboBox4.Items.Add(1.5);
            comboBox4.Items.Add(2);

            // initialize the text field of the stop bits combo box
            comboBox4.Text = "1";

        }   // end loadStopBitsComboBox() definition

        private void loadParityComboBox()
        {
            // local variable declarations

            /* Initialize ParityComboBox GUI control
             */
            comboBox5.Items.Add("NONE");
            comboBox5.Items.Add("ODD");
            comboBox5.Items.Add("EVEN");
            comboBox5.Items.Add("MARK");
            comboBox5.Items.Add("SPACE");

            // initialize the text field of the parity combo box
            comboBox5.Text = "NONE";

        }   // end loadParityComboBox() definition

        private void loadHandshakeComboBox()
        {
            // local variable declarations

            /* Initialize HandshakeComboBox GUI control
             */
            comboBox6.Items.Add("RTS");
            comboBox6.Items.Add("XonXoff");
            comboBox6.Items.Add("NONE");

            // initialize the text field of the handshake combo box
            comboBox6.Text = "NONE";

        }   // end loadHandshakeComboBox() definition

        private void configureComPort()
        {
            // configure serial port to selected COM port
            serialPort1.PortName = comboBox1.Text;
        }   

        private void configureBitRate()
        {
            // configure serial baud rate
            serialPort1.BaudRate = Int32.Parse(comboBox2.Text);
        }

        private void configureDataBits()
        {
            // configure serial port to 7 or 8 data bits
            serialPort1.DataBits = Int32.Parse(comboBox3.Text);
        }
        private void configureStopBits()
        {
            // configure serial port to 1, 1.5 or 2 stop bits
            if( comboBox4.Text.Equals("1") )
                serialPort1.StopBits = System.IO.Ports.StopBits.One;
            else if (comboBox4.Text.Equals("1.5")) // 1.5 doesn't configure
                serialPort1.StopBits = System.IO.Ports.StopBits.One;
            else if (comboBox4.Text.Equals("2"))
                serialPort1.StopBits = System.IO.Ports.StopBits.Two;
            else
                serialPort1.StopBits = System.IO.Ports.StopBits.One;
        }

        private void configureParity()
        {
            // configure parity to none, odd, even, mark or space
            if (comboBox5.Text.Equals("NONE"))
                serialPort1.Parity = System.IO.Ports.Parity.None;
            else if (comboBox5.Text.Equals("ODD"))
                serialPort1.Parity = System.IO.Ports.Parity.Odd;
            else if (comboBox5.Text.Equals("EVEN"))
                serialPort1.Parity = System.IO.Ports.Parity.Even;
            else if (comboBox5.Text.Equals("MARK"))
                serialPort1.Parity = System.IO.Ports.Parity.Mark;
            else if (comboBox5.Text.Equals("SPACE"))
                serialPort1.Parity = System.IO.Ports.Parity.Space;
            else
                serialPort1.Parity = System.IO.Ports.Parity.None;
        }

        private void configureHandshaking()
        {
            // configure handshaking to RTS, XonXoff or None
            if (comboBox5.Text.Equals("RTS"))
                serialPort1.Handshake = System.IO.Ports.Handshake.RequestToSend;
            else if (comboBox5.Text.Equals("XonXoff"))
                serialPort1.Handshake = System.IO.Ports.Handshake.XOnXOff;
            else if (comboBox5.Text.Equals("NONE"))
                serialPort1.Handshake = System.IO.Ports.Handshake.None;
            else
                serialPort1.Handshake = System.IO.Ports.Handshake.None;
        }

        private void displayCommParameters()
        {
            textBox1.Text = comboBox1.Text + " is the selected COM port\r\n";
            textBox1.AppendText("Bit Rate is " + comboBox2.Text + "\r\n");
            textBox1.AppendText("Data Bit setting is " + comboBox3.Text + "\r\n");
            textBox1.AppendText("Stop Bits setting is " + comboBox4.Text + "\r\n");
            textBox1.AppendText("Selected parity setting is " + comboBox5.Text + "\r\n");
            textBox1.AppendText("Provisioned handshaking is " + comboBox6.Text + "\r\n");
        }   // end displayCommParameters() definition

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New COM port selected\r\n";
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New bit rate selected\r\n";    
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New data bit number selected\r\n";            
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New stop bit number selected\r\n";         
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New parity selected\r\n";           
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            //textBox1.Text = "New handshaking selected\r\n";
        }

         private void button1_Click(object sender, EventArgs e)
        {
            // close the serialport1 COM port before configuring if open
             if( serialPort1.IsOpen )
                serialPort1.Close();
           
            /* The communication parameters for serialport1 control is 
             * provisioned according the the user selections contained in the
             * GUI Combo Box selections.
             */
            configureComPort();
            configureBitRate();
            configureDataBits();
            configureStopBits();
            configureParity();
            configureHandshaking();

            // enable the serialport1 COM port
            serialPort1.Open();

            // display user provisioned comm parameters in Textbox
            displayCommParameters();

            // display message that COM port is provisioned and opened
            textBox1.AppendText("\r\nSerialPort1 is provisioned and ready to use\r\n");     
        }

         private void label1_Click(object sender, EventArgs e)
         {
         }
    }
}
